<?php
/**
 * @file
 */

namespace Drupal\cocktail_module\Plugin\Block;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Url;

/**
 * Provides a 'Cocktail' Block.
 *
 * @Block(
 *   id = "cocktail_block",
 *   admin_label = @Translation("Cocktail Block"),
 *   category = @Translation("Cocktail"),
 * )
 */
class CocktailBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $client = \Drupal::service('http_client');
    $rest = $client->request('GET', 'https://www.thecocktaildb.com/api/json/v1/1/random.php');

    $random_cocktail = Json::decode($rest->getBody()->getContents(), TRUE);
    $measurements = [];
    $ingredients = [];
    foreach ($random_cocktail['drinks'][0] as $info => $value) {
      $cocktail_image = $random_cocktail['drinks'][0]['strDrinkThumb'];
      $cocktail_name = $random_cocktail['drinks'][0]['strDrink'];
      $is_alcoholic = $random_cocktail['drinks'][0]['strAlcoholic'];
      $cocktail_glass = $random_cocktail['drinks'][0]['strGlass'];

      $cocktail_instruction = $random_cocktail['drinks'][0]['strInstructions'];
      if (strpos($info, 'strMeasure') === 0 && !empty($value)) {
        $measurements[$info] = $value;
      }
      if (strpos($info, 'strIngredient') === 0 && !empty($value)) {
        $ingredients[$info] = $value;
      }
    }

    // The Content that loads before the link.
    $build['content'] = [
      '#prefix' => '<div id="cocktail-content">',
      'cocktail_recipe' => [
        '#theme' => 'my-template',
        '#cocktail_name' => $cocktail_name,
        '#is_alcoholic' => $is_alcoholic,
        '#cocktail_glass' => $cocktail_glass,
        '#ingredients' => $ingredients,
        '#measurements' => $measurements,
        '#instruction' => $cocktail_instruction,
        '#uri' => $cocktail_image,
        '#cache' => [
          'max-age' => 0,
        ],
      ],
      '#suffix' => '</div>',
    ];

    // A required js library, which can execute ajax commands in frontend.
    $build['#attached']['library'][] = 'core/drupal.ajax';

     $build['link'] = [
       '#type' => 'link',
       '#title' => 'Next Option',
       '#url' => Url::fromRoute('cocktail_module.getCocktail'),
       '#attributes' => [
         'class' => 'use-ajax',
        ],
      ];

    return $build;
  }
}
