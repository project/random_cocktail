README.txt for Random Cocktail Generator
---------------------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Maintainers

INTRODUCTION
------------

The Random Cocktail Generator module contains cocktail recipes

INSTALLATION
------------

 - Install and enable the random_cocktail module as you would normally install any Drupal module.
   module. Visit https://www.drupal.org/node/1897420 for further information.
 - In the Manage administrative menu, navigate to Structure > Block layout.
 - Place the Cocktail Block in the section where you would like the content show by clicking the "Place block" button.
 - Search for "Cocktail Block" then click "Place block".

Author/Maintainers
------------------

 - Alberto Martinez (01martinez) - https://www.drupal.org/u/01martinez
